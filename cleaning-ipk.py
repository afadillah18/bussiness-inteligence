import pandas as pd
import re

def clean_ipk(ipk):
    # Mengganti koma menjadi titik
    ipk = ipk.replace(',', '.')

    # Membersihkan IPK menggunakan pendekatan sebelumnya
    cleaned_ipk = re.sub(r'[^0-9.]', '', ipk)
    if cleaned_ipk.count('.') > 1:
        cleaned_ipk = cleaned_ipk.replace('.', '', cleaned_ipk.count('.') - 1)
    if '.' in cleaned_ipk:
        integer_part, decimal_part = cleaned_ipk.split('.')
        decimal_part = decimal_part[:2]
        cleaned_ipk = f'{integer_part}.{decimal_part}'
    
    return cleaned_ipk

def clean_ipk_csv(input_csv, output_csv):
    try:
        # Membaca file CSV menggunakan pandas
        df = pd.read_csv(input_csv)

        # Mengecek nama-nama kolom
        print("Nama Kolom:", df.columns)

        # Membersihkan dan mengubah format IPK
        if 'IPK' in df.columns:
            df['IPK'] = df['IPK'].apply(lambda x: clean_ipk(str(x)))

            # Menyimpan hasil pembersihan ke file CSV output
            df.to_csv(output_csv, index=False)
            print("Pembersihan berhasil. Hasil disimpan di", output_csv)
        else:
            print("Kolom 'IPK' tidak ditemukan dalam file CSV.")
    except Exception as e:
        print("Terjadi kesalahan:", e)

# Ganti 'input-ipk.csv' dan 'output_cleaned.csv' dengan nama file yang sesuai
clean_ipk_csv('input-ipk.csv', 'output_cleaned.csv')
