import csv

def load_mapping_from_csv(csv_file):
    """
    Membaca data kabupaten dan provinsi dari file CSV.
    Mengembalikan kamus pemetaan.
    """
    kabupaten_provinsi_mapping = {}
    with open(csv_file, newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        next(reader)  # Skip header if exists
        for row in reader:
            kabupaten_provinsi_mapping[row[0]] = row[1]
    return kabupaten_provinsi_mapping

def mapping_kabupaten_ke_provinsi(kabupaten, mapping):
    """
    Fungsi untuk melakukan pemetaan data kabupaten ke data provinsi.
    """
    return mapping.get(kabupaten, 'Provinsi Tidak Diketahui')

def process_input_file(input_file, output_file, mapping):
    """
    Memproses file input dan menyimpan hasilnya ke file output.
    """
    output_data = []
    with open(input_file, newline='', encoding='utf-8') as infile:
        reader = csv.reader(infile)
        header = next(reader, None)
        if header:
            header.append('Provinsi')  # Menambahkan kolom provinsi ke header
            output_data.append(header)
        
        for row in reader:
            kabupaten = row[0]
            provinsi = mapping_kabupaten_ke_provinsi(kabupaten, mapping)
            row.append(provinsi)
            output_data.append(row)

    with open(output_file, 'w', newline='', encoding='utf-8') as outfile:
        writer = csv.writer(outfile)
        writer.writerows(output_data)

def main():
    # Ganti 'input_data.csv' dan 'output_data.csv' sesuai dengan nama file Anda
    input_file = 'input_data.csv'
    output_file = 'output_data.csv'

    # Ganti 'file_mapping.csv' dengan nama file CSV pemetaan Anda
    mapping_file = 'file_mapping.csv'
    
    # Memuat pemetaan dari file CSV
    kabupaten_provinsi_mapping = load_mapping_from_csv(mapping_file)

    # Memproses file input dan menyimpan hasilnya ke file output
    process_input_file(input_file, output_file, kabupaten_provinsi_mapping)

if __name__ == "__main__":
    main()
